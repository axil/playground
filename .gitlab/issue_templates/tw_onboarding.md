## Technical Writing team onboarding

### Manager tasks

The manager for the person being onboarded must complete these tasks:

1. [ ] Request that new team member be added to `docs@gitlab.com` and
   `ux-department@gitlab.com` email groups, and `@docsteam` user group in Slack
   (they'll be added to `#docs`, `#docs-team`, and `#docs-comments`).
1. [ ] Add the new team member to the [`gl-docsteam` group](https://gitlab.com/groups/gl-docsteam/-/group_members)
   on GitLab.com.
1. [ ] Determine and assign secondary group for shadowing.
1. [ ] Determine and assign group for trainee (for example, to work towards
   being assigned as its technical writer).
1. [ ] Determine and schedule further group assignments.
1. [ ] Add the new team member to the following configuration files:
   - [`sites/handbook/source/includes/product/_categories-names.erb`](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/sites/handbook/source/includes/product/_categories-names.erb)

### Team member tasks

Welcome to the Technical Writing team! Here are your technical writing-related onboarding tasks:

#### Week 1-2

1. [ ] Say "hi" to the rest of the Technical Writing team in the #tw-team
   channel in Slack. Tell us a bit more about yourself by using the
   [Team Agenda doc](https://docs.google.com/document/d/1XRyVjR5G21Amq4QqJs9jbV0BVQquiBAyPR257-hT1JY/edit),
   and then during the first team meeting you're able to attend.
1. [ ] Join the team's [Slack channels](https://about.gitlab.com/handbook/engineering/ux/technical-writing/#slack-channels),
   and #docs-team-onboarding. In the latter channel, ask any questions that
   arise during your onboarding. Don't worry about asking seemingly basic
   questions; everyone has joined the team bringing their own knowledge and
   experience, and some of our practices may not be obvious or documented.
   You can improve our onboarding (or other process documentation) by creating
   MRs, issues, or mentioning ideas in the Slack channel.
1. [ ] Read the following to become familiar with how technical writing is done
   at GitLab:
   - [ ] [Technical Writing Handbook](https://about.gitlab.com/handbook/product/technical-writing/).
   - [ ] Review the [GitLab Technical Writing Fundamentals](https://about.gitlab.com/handbook/engineering/ux/technical-writing/fundamentals/).
   - [ ] [GitLab Documentation guidelines](https://docs.gitlab.com/ce/development/documentation/index.html)
     and the various pages linked from the introduction. Note that there are
     [Company writing style guidelines](https://about.gitlab.com/handbook/communication/#writing-style-guidelines)
     in addition to our Documentation Style Guide.
      - [ ] Per <https://docs.gitlab.com/ee/development/documentation/#source-files-and-rendered-web-locations>,
        begin to familiarize yourself with the `gitlab` project's `doc`
        directory.
      - [ ] In addition to the projects where content is maintained (like
        `gitlab` and `runner`), review these key projects the team uses:
        - <https://gitlab.com/gitlab-org/technical-writing>: Process issues.
        - <https://gitlab.com/gitlab-org/gitlab-docs>: Documentation site
          issues (including the frontend, backend, and CI).
   - [ ] Review these processes, noting the mentions of "documentation" and
     "technical writer"/"technical writing":
     - [GitLab Release Posts](https://about.gitlab.com/handbook/marketing/blog/release-posts/#tw-lead)
     - [UX design](https://about.gitlab.com/handbook/engineering/ux/ux-designer/#working-on-issues)
     - [Product development](https://about.gitlab.com/handbook/product-development-flow/)

     For the Release Post items, reviews of each feature's details are reviewed by
     the technical writer assigned to each stage/group. This is specified only in the [MR template](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/.gitlab/merge_request_templates/Release-Post-Item.md), and
     not in the Handbook page.
1. [ ] [Set up your computer](https://about.gitlab.com/handbook/engineering/ux/technical-writing/setup/)
   for writing and previewing GitLab product documentation.
1. [ ] Familiarize yourself with GitLab's [merge requests guidelines](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html).
1. [ ] The Technical Writing team uses the Geekbot app in Slack to run
   asynchronous standups, which include the 'Morning Stretch' and
   'Wednesday Weekly' questions. Ask your onboarding buddy who can add you to
   the application.
1. [ ] If you want to start feeling productive beyond your training, ask your
   onboarding buddy for a simple task to get you started.
1. [ ] Verify you have access to view the UX Meetings calendar, and add the calendar. Go to your calendar, left sidebar, go to Other calendars, press the + sign, Subscribe to calendar, and enter in the search field `gitlab.com_9psh26fha3e4mvhlrefusb619k@group.calendar.google.com` and then press Enter on your keyboard. NOTE: Please do NOT remove any meetings from this calendar or any other shared calendars, as it removes the event from everyone's calendar.
1. [ ] As you're reading, see if you can find the answers to
   [these questions](https://gitlab.com/gitlab-org/technical-writing/-/blob/main/onboarding/tw_quiz.md). If you're unsure of
   something, ask your buddy or post in Slack. When you're ready, you can
   [check your answers](https://gitlab.com/gitlab-org/technical-writing/-/blob/main/onboarding/answer_key.md).

In the following sections, link to the issues and MRs you're assigned to or
that you create. Edit the issue and include those links as soon as they exist;
don't wait for the end of the milestone.

#### First release - Shadow stage groups

1. [ ] For the first release cycle beginning after your start date (the first
   18th), shadow your buddy's work in [their most active Stage Group], plus one
   other stage group/writer, decided with your manager and the team. During
   shadowing, the established technical writers proactively share relevant issues,
   merge requests, and communications with you by creating/using a
   #tw-onboarding-<groupname> channel, and answer questions you have that arise
   in this context.
1. [ ] Read about all stages and groups, and discuss with your manager whether
   there's a particular group that will need your coverage as technical writer,
   or multiple groups you may be interested in.

   - [ ] List group 1 here:
   - [ ] List group 2 here:

1. [ ] Link to any issues you are assigned here:
1. [ ] Link to any merge requests you are assigned here:

#### Second release - Trainee

1. [ ] For the second release cycle that begins after your start date (the
   second 18th), unless your manager extends the shadowing phase, act as a
   trainee for a single group (as assigned by your manager). The intent is to
   take on the group as its technical writer for the next release. List the
   group name here:
   The technical writer assigned to that group assigns substantial parts of the
   work to you for this group (roughly half of the groups's reviews of MRs with
   documentation, UI text, and release post content; a small but substantial
   documentation authoring project; a few minor documentation improvement
   projects/fixes).
1. [ ] Get to know the group: read about the features, read the documentation,
   try some of the features, review the group's vision page, review its
   docs-only issue board, read all the documentation applicable to it, and set
   up a meeting with the group Product Manager (PM) to get to know them, their
   priorities, how they'd recommend staying in sync about docs-only issue
   priorities on the board (and otherwise working together), and any upcoming
   documentation needs.
1. [ ] Create one or more issues and one MR as a result of your documentation
   review, and link them here. Be sure to use the appropriate stage, group,
   `documentation`, and `Technical Writing` labels:
1. [ ] Add the links to any issues you're assigned as part of the group for
   this milestone:
1. [ ] Add the links to any other merge requests you're assigned or authored
   here as part of the group:
1. [ ] Add the links to other issues to which you're assigned or MRs you
   authored or reviewed that don't pertain to the group (if any):
1. [ ] Plan to address one or more docs-only issues for this group during the
   next milestone. Get feedback and agreement from the PM and your manager.
   Link the issues here:

The trainee phase may be extended by an additional milestone at the discretion
of the manager.

#### Third release - Assigned with developer permissions and coach

1. [ ] For the third release cycle, you assume the full role of technical
   writer for the group, except that the former technical writer assigned to
   the group is now the coach, who will review all your work (including reviews
   you perform of other authors) before it is merged. They may share the burden
   of these reviews with other technical writers.
1. [ ] Submit an access request to become a maintainer for these projects:
   - [`gitlab`](https://gitlab.com/gitlab-org/gitlab)
   - [`omnibus-gitlab`](https://gitlab.com/gitlab-org/omnibus-gitlab)
   - [`charts/gitlab`](https://gitlab.com/gitlab-org/charts/gitlab)
   - The [`gitlab-runner`](https://gitlab.com/gitlab-org/gitlab-runner) groups:
     - [`runner-docs-maintainers`](https://gitlab.com/groups/gitlab-com/runner-docs-maintainers/-/group_members?sort=access_level_desc).
     - [`runner-maintainers`](https://gitlab.com/groups/gitlab-com/runner-maintainers/-/group_members?sort=access_level_desc)
   - [`gitlab-development-kit`](https://gitlab.com/gitlab-org/gitlab-development-kit)
   - [`gitlab-docs`](https://gitlab.com/gitlab-org/gitlab-docs)
   - [`technical-writing`](https://gitlab.com/gitlab-org/technical-writing)
   
   Use the [Single person access request template](https://gitlab.com/gitlab-com/access-requests/issues/new)
   and tag your manager. Going forward, if the manager approves the access request, you can
   merge content without a coach as the technical writer for the group.
   Otherwise, your developer permissions and coach's collaboration may continue
   through another release.
1. [ ] When you have maintainer access to the previously listed projects, submit an MR to update your [team
member](https://about.gitlab.com/company/team/)
 file with your project permissions. This change will have an effect:
   - On projects here: https://about.gitlab.com/handbook/engineering/projects/.
   - On the GitLab Review Workload Dashboard page here: https://gitlab-org.gitlab.io/gitlab-roulette/?sortKey=stats.avg7&order=-1&hourFormat24=true&visible=maintainer%7Cdocs.
1. [ ] Submit an MR to add yourself as a `docs.gitlab.com` administrator to the Google Search Console.
   Follow the instructions to add an [HTML `meta` tag](https://support.google.com/webmasters/answer/9008080?hl=en&ref_topic=9455938#zippy=%2Chtml-tag):

   1. Go to <https://search.google.com/search-console>.
   1. In **URL Prefix**, enter `https://docs.gitlab.com` and select **Continue**.
   1. Expand the **HTML Tag** section, and copy the code given.
   1. Open an MR to add the tag and your name to the list at <https://gitlab.com/gitlab-org/gitlab-docs/-/blob/4b87b706ee1f32d2fa25e59d604750ca408f2359/layouts/head.html#L51>.
   1. Submit the MR to your manager for approval and merge.

1. [ ] Add the links to other issues to which you're assigned or MRs you
   authored or reviewed that don't pertain to the group, if any:

/label ~"onboarding" ~"Technical Writing"
